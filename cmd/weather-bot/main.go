package main

import (
	"net/http"
	"net/url"
	"time"
	"weather-bot/pkg/config"
	"weather-bot/pkg/iqair"
	"weather-bot/pkg/rest"
	"weather-bot/pkg/template"
	"weather-bot/pkg/tgclient"

	"github.com/heptiolabs/healthcheck"
	log "github.com/sirupsen/logrus"
)

func main() {

	// Init service
	cfg := config.AppConfig

	tmpl, err := template.LoadTemplate(cfg.TemplatePath)
	if err != nil {
		log.Warn("Unable to load message template: %s", err.Error())
	}

	iqairClient := iqair.NewIqairClient(cfg.IqairUrl, cfg.IqairToken)
	tgClient, err := tgclient.NewTgClient(cfg.TgApiUrl, cfg.TgApiToken, iqairClient, tmpl, cfg)
	if err != nil {
		log.Fatalf("Unable to initialize tg client: %s", err.Error())
	}
	httpClient := rest.NewHttpClient(iqairClient, cfg)

	log.Info("Start listening for telegram events...")

	// Run Clients
	go httpClient.Listen()
	go tgClient.Listen()

	//Run healthchecks
	rawUrl, err := url.Parse(cfg.IqairUrl)
	if err != nil {
		log.Error("Error parsing URL:", err.Error())
	}
	v := url.Values{}
	v.Add("key", cfg.IqairToken)
	requestURL := rawUrl.JoinPath("/countries")
	requestURL.RawQuery = v.Encode()

	health := healthcheck.NewHandler()
	health.AddReadinessCheck(
		"http-get-check",
		healthcheck.HTTPGetCheck(requestURL.String(), 1*time.Second))
	health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(100))
	http.ListenAndServe(":8086", health)

	return
}
