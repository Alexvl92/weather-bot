# First Helm Chart

## Preparation
```bash
git clone https://gitlab.com/ksxack/weather-bot.git
cd weather-bot
cd charts
cp -r 01-dummy/ 02-values
```

## Create Chart
```bash
helm template weather 02-values/
helm template weather-bot 02-values/
```

## Deploy first Helm Release
```bash
kubectl delete ns weather-bot
helm -n weather-dev upgrade --install weather 02-dummy/ --create-namespace
kubectl -n weather-dev get po
```


