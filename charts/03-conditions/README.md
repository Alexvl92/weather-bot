# First Helm Chart

## Preparation
```bash
git clone https://gitlab.com/ksxack/weather-bot.git
cd weather-bot
cd charts
cp -r 02-values/ 03-conditions
```

## Create Chart
```bash
helm template weather-bot 03-values/
helm template weather-bot 03-conditions/ -n weather-123
```
